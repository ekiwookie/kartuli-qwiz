import { defineStore } from 'pinia'

function setLocalStorageObject(key, data) {
  localStorage.setItem(key, JSON.stringify(data))
}

function getLocalStorageObject (key) {
    try {
        return JSON.parse(localStorage.getItem(key)) || {}
    } catch (error) {
        console.error(`error to get object from localStorage ${key}`)
        return {}
    }
}

export const useStore = defineStore('store', {
  state: () => ({
    settings: {},
  }),
  getters: {
    settingLimit() {
      return this.settings.numLimit || 1000
    },
    settingMode() {
      return this.settings.mode || 'kartuli'
    },
    settingThemeDark() {
      return this.settings.themeDark || false
    },
    settingUseDelitimer() {
      return this.settings.useDelitimer || false
    }
  },
  actions: {
    init() {
      this.settings = getLocalStorageObject('settings')
      this.enableTheme()
    },
    enableTheme() {
      document.getElementsByTagName('html')[0].setAttribute(
        'data-theme',
        this.settingThemeDark ? 'dark' : 'light'
      )
    },
    setLimit(value) {
      this.settings.numLimit = value
      this.saveSettings()
    },
    setMode(value) {
      this.settings.mode = value
      this.saveSettings()
    },
    setThemeDark(value) {
      this.settings.themeDark = value
      this.enableTheme()
      this.saveSettings()
    },
    setUseDelitimer(value) {
      this.settings.useDelitimer = value
      this.saveSettings()
    },
    saveSettings() {
      setLocalStorageObject('settings', this.settings)
    },
    getStats(mode) {
      return {
        total: parseInt(localStorage.getItem(`stats:${mode}:total`)) || 0,
        correct: parseInt(localStorage.getItem(`stats:${mode}:correct`)) || 0
      }
    },
    addAnswer(correct, data) {
      const currentStats = this.getStats(data.mode)
      localStorage.setItem(`stats:${data.mode}:total`, currentStats.total + 1)
      if (correct) {
        localStorage.setItem(`stats:${data.mode}:correct`, currentStats.correct + 1)
      }
    },
    clearStats() {
      localStorage.setItem('stats:kartuli:total', null)
      localStorage.setItem('stats:kartuli:correct', null)
      localStorage.setItem('stats:numeric:total', null)
      localStorage.setItem('stats:numeric:correct', null)
    }
  },
})
