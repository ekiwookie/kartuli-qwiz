import { getExponentNum } from '@/utils/common'
import kartuli from '@/utils/strings'


export default function getHundreds(num) {
  let hundreds = kartuli.hundreds[getExponentNum(num, 2)]
  if (!hundreds) return
  if (!Boolean(num % 100)) {
    hundreds += 'ი'
  }
  return hundreds

  let ret = []
  ret.push(getExponentNum(num, 2))
  return ret
}
