import getTwenties from '@/utils/twenties'
import getHundreds from '@/utils/hundreds'


export default function getTsousands(num) {
  const atas = !Math.floor(num % 1000) ? 'ათასი' : 'ათას'
  const tsousands = Math.floor(num / 1000)
  if (!tsousands) return
  if (tsousands < 2) {
    return [atas]
  }
  return [
    getHundreds(tsousands),
    getTwenties(tsousands),
    atas
  ].join(' ')
}
