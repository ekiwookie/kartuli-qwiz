import kartuli from '@/utils/strings'
import { useStore } from '@/stores'

function getTwentiesNum(num) {
  return Math.floor(num % 100 / 20)
}

function getTwentiesRestNum(num){
  return num % 100 % 20
}

export default function getTwenties(num, useDelitimer=true) {
  const store = useStore()

  const twenties = getTwentiesNum(num)
  const twentiesRest = getTwentiesRestNum(num)

  let ret = []

  if (twenties > 0) {
    if (!twentiesRest) {
      ret.push(kartuli.twenties[twenties] + 'ი')
    } else {
      ret.push(kartuli.twenties[twenties])
      ret.push('და')
    }
  }
  if (twentiesRest) {
    ret.push(kartuli.lessTwenty[twentiesRest])
  }

  return ret.join(store.settingUseDelitimer ? '‧' : '')
}
