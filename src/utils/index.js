import getTwenties from '@/utils/twenties'
import getHundreds from '@/utils/hundreds'
import getTsousands from '@/utils/thousands'


export default function numToString(num) {
  return [
    getTsousands(num),
    getHundreds(num),
    getTwenties(num)
  ].join(' ')
}


