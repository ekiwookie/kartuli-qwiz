export function getExponentNum(num, ex) {
    return Math.floor(num % (10 ** (ex + 1)) / (10 ** ex))
}
