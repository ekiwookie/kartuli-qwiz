import { fileURLToPath, URL } from 'node:url'

import { defineConfig } from 'vite'
import vue from '@vitejs/plugin-vue'

const packageJSON = require('./package.json')

// https://vitejs.dev/config/
export default defineConfig({
  define: {
    VERSION: JSON.stringify(packageJSON.version),
  },
  plugins: [vue()],
  resolve: {
    alias: {
      '@': fileURLToPath(new URL('./src', import.meta.url))
    }
  }
})
