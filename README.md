# KartuliQuiz

https://kartuli.ekiwookie.ninja/

Тренажер показывает случайную карточку с вопросом, которую можно виртуально перевернуть, сверить со своим ответом и нажать на кнопку, соответствующую правильности предпологаемого ответа. А потом можно глянуть статистику. Вот собственно и все. გაგიმარჯოს.

## Customize configuration

See [Vite Configuration Reference](https://vitejs.dev/config/).

## Project Setup

```sh
npm install
```

### Compile and Hot-Reload for Development

```sh
npm run dev
```

### Compile and Minify for Production

```sh
npm run build
```
